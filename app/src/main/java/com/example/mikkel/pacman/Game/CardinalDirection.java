package com.example.mikkel.pacman.Game;

public enum CardinalDirection
{
    North,
    West,
    East,
    South
}
