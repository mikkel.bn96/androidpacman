package com.example.mikkel.pacman.Utils;

import android.content.res.Resources;
import android.util.TypedValue;

public class ResourceUtils
{
    public static float GetFloat(int id, Resources resources)
    {
        TypedValue typedValue = new TypedValue();
        resources.getValue(id,typedValue,true);
        return typedValue.getFloat();
    }
}
