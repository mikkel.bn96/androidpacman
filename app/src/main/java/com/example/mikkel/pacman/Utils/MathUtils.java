package com.example.mikkel.pacman.Utils;

public class MathUtils
{
    public static Vector2 lerp(Vector2 v1, Vector2 v2, float lerpValue)
    {
        float x = v1.getX() + lerpValue * (v2.getX() - v1.getX());
        float y = v1.getY() + lerpValue * (v2.getY() - v1.getY());
        return new Vector2(x,y);
    }

    public static int BooleanToInt(Boolean value)
    {
        return value ? 1 : 0;
    }

    public static float Pow (float value, int powerOf)
    {
        float number = value;
        for (int i = 1; i < powerOf; i++) {
            number = number * value;
        }
        return number;
    }

    public static float GetBiggest(float v1, float v2)
    {
        return v1 > v2 ? v1 : v2;
    }

    public static float GetSmallest(float v1, float v2)
    {
        return v1 < v2 ? v1 : v2;
    }

    public static float Clamp(float value, float min, float max)
    {
        float finalValue = value;
        if(finalValue < min)
            finalValue = min;
        else if(finalValue > max)
            finalValue = max;
        return finalValue;
    }

    public static int FloatTOAlpha(float value)
    {
        return (int)MathUtils.Clamp(255f * value,0,255f);
    }
}
