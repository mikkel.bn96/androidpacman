package com.example.mikkel.pacman.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.mikkel.pacman.Game.Animator;
import com.example.mikkel.pacman.Game.Game;
import com.example.mikkel.pacman.Interfaces.Destroyable;
import com.example.mikkel.pacman.Interfaces.Drawable;
import com.example.mikkel.pacman.Interfaces.OnCollision;
import com.example.mikkel.pacman.Interfaces.Resetable;
import com.example.mikkel.pacman.Interfaces.Updateable;
import com.example.mikkel.pacman.Views.GameView;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class GameObject implements Drawable,Destroyable,Resetable,Updateable,OnCollision
{
    private Bitmap bitmap;
    private int bitmapSource;
    private GameView gameView;
    private float alpha = 1;

    public Transform Transform;
    public Game Game;
    public Context Context;
    public boolean Destroying;


    private ArrayList<Component> components = new ArrayList<>();

    public GameObject(Context context, int bitmapId, GameView gameView, Game game)
    {
        this.gameView = gameView;
        Context = context;
        bitmap = BitmapFactory.decodeResource(context.getResources(),bitmapId);
        Transform = new Transform(gameView,game,this);
        bitmapSource = bitmapId;
        this.Game = game;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
        Game.invalidate();
    }

    public int getBitmapSource() {
        return bitmapSource;
    }

    public void setBitmapSource(int bitmapSource) {
        this.bitmapSource = bitmapSource;
        Transform.setSize(Transform.getSize());
        Transform.setRotation(Transform.getRotation());
    }

    public GameView getGameView() {
        return gameView;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public <T extends Component> T getComponent(Class<T> type)
    {

        for (int i = 0; i < components.size(); i++) {
            if(type.isInstance(components.get(i)))
                return (T)components.get(i);
        }
        return null;
    }

    public <T extends Component> T addComponent(T component)
    {
        components.add(component);
        return component;
    }

    @Override
    public void destroy(Context context) {

    }

    @Override
    public void draw(Canvas canvas) {

    }

    @Override
    public void reset(Context context) {

    }

    @Override
    public void update(Context context, float deltaTime) {
        for (Component c : components)
        {
            c.update(context,deltaTime);
        }
    }

    @Override
    public void onCollision(GameObject collision)
    {
        for (Component c : components)
        {
            if(OnCollision.class.isInstance(c))
                ((OnCollision)c).onCollision(collision);
        }
    }
}
