package com.example.mikkel.pacman.Game;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

import com.example.mikkel.pacman.R;

import java.util.HashMap;

public class SoundManager {
    private static final SoundManager ourInstance = new SoundManager();

    private MediaPlayer mediaPlayer;
    private SoundPool soundPool;
    private HashMap<Integer,Integer> soundEffects = new HashMap<>();

    private boolean initialized;

    public static SoundManager getInstance() {
        return ourInstance;
    }

    private SoundManager()
    {
    }

    public void initialize(Context context)
    {
        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC,100);
        soundEffects = new HashMap<>();
        initialized = true;
        addSound(context,R.raw.eat);
    }

    private void addSound(Context context,int id)
    {
        int value = soundPool.load(context, id,100);
        soundEffects.put(id,value);
    }

    public void terminate()
    {
        initialized = false;
        soundEffects.clear();
        
        if(soundPool != null)
            soundPool.release();
        if(mediaPlayer != null)
            mediaPlayer.stop();
    }

    public void playMusic(Context context, SoundEffectId id)
    {
        mediaPlayer = MediaPlayer.create(context,id.value());
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
        mediaPlayer.setVolume(0.2f,0.2f);
    }

    public void pauseMusic()
    {
        if(mediaPlayer != null && mediaPlayer.isPlaying())
            mediaPlayer.pause();
    }

    public void resumeMusic()
    {
        if(mediaPlayer != null && !mediaPlayer.isPlaying())
            mediaPlayer.start();
    }

    public void playSound(SoundEffectId id)
    {
        if(initialized && soundPool != null)
        {
            int value = soundEffects.get(id.value());
            soundPool.play(value,1,1,1,0,1);
        }
    }

    public void pauseSounds()
    {
        if(initialized && soundPool != null)
            soundPool.autoPause();
    }

    public void resumeSounds()
    {
        if(initialized && soundPool != null)
            soundPool.autoResume();
    }


}
