package com.example.mikkel.pacman.Game;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.mikkel.pacman.R;
import com.example.mikkel.pacman.Utils.BoxCollider;
import com.example.mikkel.pacman.Utils.GameObject;
import com.example.mikkel.pacman.Utils.MathUtils;
import com.example.mikkel.pacman.Utils.ResourceUtils;
import com.example.mikkel.pacman.Utils.Vector2;
import com.example.mikkel.pacman.Utils.Vector2Int;
import com.example.mikkel.pacman.Views.GameView;

public class Player extends GameObject
{
    private Vector2 oldDirection;
    private Vector2Int oldPosition;
    private Animator animator;

    public Player(Context context, int bitmapId, GameView gameView, Game game) {
        super(context, bitmapId, gameView, game);
        oldDirection = Transform.getRotation();
        float size = ResourceUtils.GetFloat(R.dimen.playerScale,context.getResources());
        Transform.setSize(new Vector2(size,size));

        animator = addComponent(new Animator(this));

        animator.startAnimation(new Animation("Eat",30,new int[]{
                R.drawable.pacman_eat_000,R.drawable.pacman_eat_001,R.drawable.pacman_eat_002,
                R.drawable.pacman_eat_003,R.drawable.pacman_eat_004,R.drawable.pacman_eat_005,
                R.drawable.pacman_eat_006,R.drawable.pacman_eat_007,R.drawable.pacman_eat_008,
                R.drawable.pacman_eat_009,R.drawable.pacman_eat_010,R.drawable.pacman_eat_011}));
        animator.stop();
        addComponent(new BoxCollider(this,new Vector2Int(30,30)));
    }

    @Override
    public void destroy(Context context) {

    }

    @Override
    public void draw(Canvas canvas) {
        Bitmap bitmap = getBitmap();
        Vector2Int position = Transform.getPosition();
        Paint paint = new Paint();
        paint.setAlpha(MathUtils.FloatTOAlpha(getAlpha()));
        canvas.drawBitmap(getBitmap(),position.getX() - getBitmap().getWidth()/2,position.getY() - getBitmap().getHeight()/2,paint);
    }

    @Override
    public void reset(Context context) {

    }

    @Override
    public void update(Context context,float deltaTime)
    {
        super.update(context,deltaTime);
        oldPosition = Transform.getPosition();

        Vector2Int position = Transform.getPosition();
        Vector2Int direction = new Vector2Int(Game.getDirection().getX(),Game.getDirection().getY());
        direction.setY(-direction.getY());
        Vector2Int velocity = direction.multiply(context.getResources().getInteger(R.integer.playerSpeed) * deltaTime);
        Vector2Int newPosition = position.plus(velocity);
        Vector2Int predictedPosition = newPosition.plus(new Vector2Int(getBitmap().getWidth() / 2 * direction.getX(),
                getBitmap().getHeight() / 2 * direction.getY()));

        if(predictedPosition.getX() > 0 && predictedPosition.getX() < Game.getSize().getX() &&
                predictedPosition.getY() > 0 && predictedPosition.getY() < Game.getSize().getY())
        {
            Transform.setPosition(newPosition);
            animator.play();
        }
        else
        {
            animator.stop();
        }

        if(direction.isEqual(new Vector2Int(0,0)))
            animator.stop();

        float interpolation = ResourceUtils.GetFloat(R.dimen.playerTurnInterpolation,context.getResources());


        Vector2 dir = oldDirection;
        if(Vector2.Distance(oldDirection,direction.toVector2()) > 0.001f)
        {
            dir = MathUtils.lerp(oldDirection,direction.toVector2(),interpolation * deltaTime);
        }
        else if (oldDirection != dir)
        {
            oldDirection = direction.toVector2();
        }
        oldDirection = dir;
        Transform.setRotation(dir);

        float sizeY = Math.abs(Transform.getSize().getY());
        Transform.setSize(new Vector2(Transform.getSize().getX(),dir.getX() < 0 ? -sizeY : sizeY));
    }
}
