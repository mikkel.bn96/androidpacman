package com.example.mikkel.pacman.Utils;

public class Vector2
{
    private float x;
    private float y;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = this.x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.x = this.y;
    }

    public Vector2(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public Vector2 plus(Vector2 v2)
    {
        return new Vector2(x + v2.getX(), y + v2.getY());
    }

    public Vector2 minus(Vector2 v2)
    {
        return new Vector2(x - v2.getX(), y - v2.getY());
    }

    public Vector2 multiply(Vector2 v2)
    {
        return new Vector2(x * v2.getX(),y * v2.getY());
    }

    public Vector2 multiply(float value)
    {
        return new Vector2(x * value,y * value);
    }

    public Vector2 inverse()
    {
        return new Vector2(-x,-y);
    }

    public Boolean isEqual(Vector2 vector)
    {
        Boolean value = getX() == vector.getX() && getY() == vector.getX();
        return value;
    }

    public Vector2Int toVector2Int()
    {
        return new Vector2Int(((int) getX()), ((int) getY()));
    }

    public Vector2 normalize()
    {
        double length = Math.sqrt(getX() * getX() + getY() * getY());

        if (length != 0.0) {
            float s = 1.0f / (float)length;
            setX(getX()*s);
            setY(getY()*s);
        }
        return new Vector2(getX(),getY());
    }

    public static float Distance(Vector2 v1, Vector2 v2)
    {
        float xLength = Math.abs(v1.getX() - v2.getX());
        float yLength = Math.abs(v1.getY() - v2.getY());

        return (float) Math.sqrt(MathUtils.Pow(xLength,2) + MathUtils.Pow(yLength,2));
    }

    public static float Distance(Vector2Int v1, Vector2Int v2)
    {
        float xLength = Math.abs(v1.getX() - v2.getX());
        float yLength = Math.abs(v1.getY() - v2.getY());

        return (float) Math.sqrt(MathUtils.Pow(xLength,2) + MathUtils.Pow(yLength,2));
    }

}
