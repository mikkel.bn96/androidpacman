package com.example.mikkel.pacman;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mikkel.pacman.Game.Data;
import com.example.mikkel.pacman.Game.EnemyManager;
import com.example.mikkel.pacman.Game.Food;
import com.example.mikkel.pacman.Game.FoodManager;
import com.example.mikkel.pacman.Game.Game;
import com.example.mikkel.pacman.Game.HighscoreManager;
import com.example.mikkel.pacman.Game.Player;
import com.example.mikkel.pacman.Game.SoundManager;
import com.example.mikkel.pacman.Interfaces.Destroyable;
import com.example.mikkel.pacman.Utils.SwipeTouchListener;
import com.example.mikkel.pacman.Utils.Vector2Int;
import com.example.mikkel.pacman.Views.GameView;

public class GameActivity extends Activity
{
    private Game game;
    private boolean shuttingDown;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);

        Data.reset();

        GameView gameView = findViewById(R.id.gameView);
        game = new Game(this,gameView);
        gameView.instantiate(game);
        gameView.setOnTouchListener(new SwipeTouchListener(this)
        {
            @Override
            public void onSwipe(Vector2Int direction) {
                if(Data.getPause())
                    return;
                game.setDirection(direction);
                super.onSwipe(direction);
            }
        });

        ImageButton pauseButton = findViewById(R.id.pauseButton);
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Data.gameEnding)
                    return;
                Data.setPause(!Data.getPause());
                int imageId = Data.getPause() ? R.drawable.play : R.drawable.pauseicon;
                ((ImageButton)view).setImageResource(imageId);
                enablePauseButtons(Data.getPause());
            }
        });

        Button quitButton = findViewById(R.id.quitButton);
        quitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                destroy();
                Intent startGameIntent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(startGameIntent);
                finish();
            }
        });

        Button resumeButton = findViewById(R.id.reSumeButton);
        resumeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Data.setPause(false);
                enablePauseButtons(Data.getPause());
            }
        });

        Button continueButton = findViewById(R.id.scoreButton);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shuttingDown = true;
                EditText playerName = findViewById(R.id.playerNameEditText);
                HighscoreManager.getInstance().ApplyScore(Data.Score,playerName.getText().toString());
                Intent startGameIntent = new Intent(getApplicationContext(),MainActivity.class);
                destroy();
                startActivity(startGameIntent);
                finish();
            }
        });
        updateScore();
    }

    public void updateScore()
    {
        TextView textView = findViewById(R.id.scoreNumber);
        textView.setText(String.valueOf(Data.Score));
    }

    private void enablePauseButtons(boolean value)
    {
        Button quitButton = findViewById(R.id.quitButton);
        quitButton.setEnabled(value);
        quitButton.setVisibility(value ? View.VISIBLE : View.INVISIBLE);

        Button resumeButton = findViewById(R.id.reSumeButton);
        resumeButton.setEnabled(value);
        resumeButton.setVisibility(value ? View.VISIBLE : View.INVISIBLE);

        ImageView pausePanel = findViewById(R.id.pausePanel);
        pausePanel.setEnabled(value);
        pausePanel.setVisibility(value ? View.VISIBLE : View.INVISIBLE);
    }

    public void destroy() {
        super.onDestroy();
        game.reset(this);
        game.destroy(this);
        game = null;
        SoundManager.getInstance().terminate();
    }

    @Override
    protected void onPause() {
        if(game != null && game.getGameStarted() && !shuttingDown)
        {
            Intent startGameIntent = new Intent(getApplicationContext(),MainActivity.class);
            destroy();
            startActivity(startGameIntent);
            finish();
        }
        super.onPause();
    }
}
