package com.example.mikkel.pacman.Interfaces;

import android.content.Context;

public interface Destroyable
{
    public void destroy(Context context);
}
