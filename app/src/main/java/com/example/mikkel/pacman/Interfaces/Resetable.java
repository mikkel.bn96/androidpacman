package com.example.mikkel.pacman.Interfaces;

import android.content.Context;

public interface Resetable
{
    public void reset(Context context);
}
