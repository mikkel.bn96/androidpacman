package com.example.mikkel.pacman.Interfaces;

import android.graphics.Canvas;

public interface Drawable {
    public void draw(Canvas canvas);
}
