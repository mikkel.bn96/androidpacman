package com.example.mikkel.pacman.Game;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mikkel.pacman.GameActivity;
import com.example.mikkel.pacman.Interfaces.Destroyable;
import com.example.mikkel.pacman.Interfaces.Resetable;
import com.example.mikkel.pacman.R;
import com.example.mikkel.pacman.Utils.GameObject;
import com.example.mikkel.pacman.Utils.MathUtils;
import com.example.mikkel.pacman.Utils.Vector2;
import com.example.mikkel.pacman.Utils.Vector2Int;
import com.example.mikkel.pacman.Views.GameView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Game implements Resetable, Destroyable
{
    private Context context;
    private ArrayList<GameObject> gameObjects = new ArrayList<GameObject>();
    private ArrayList<GameObject> gameObjectsToAdd = new ArrayList<GameObject>();
    private ArrayList<GameObject> gameObjectsToRemove = new ArrayList<GameObject>();

    private Timer timer;
    private GameView gameView;
    private Vector2Int size;
    private boolean gameStarted;
    private boolean sizeSet;
    private long lastTime;
    private Vector2Int direction = new Vector2Int(0,0);
    private Player player;

    public boolean getGameStarted() {
        return gameStarted;
    }

    public boolean getinvalitate() {
        return invalitate;
    }

    private boolean invalitate;

    public Vector2Int getSize() {
        return size;
    }

    public Vector2Int getDirection() {
        return direction;
    }

    public GameView getGameView() {
        return gameView;
    }

    public void setDirection(Vector2Int direction) {
        this.direction = direction;
    }
    public ArrayList<GameObject> getGameObjects() {
        return gameObjects;
    }

    public Game(Context context, GameView gameView)
    {
        this.context = context;
        this.gameView = gameView;

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                onUpdate();
            }
        },0,16);

    }

    private void onUpdate()
    {
        //Calculates deltatime
        long time = System.nanoTime();
        float  deltaTime = ((float)(time - lastTime) / 1000000000f) * MathUtils.BooleanToInt(!Data.getPause());
        lastTime = time;

        if(Data.getPause())
            return;

        //Adds new GameObejcts
        if(gameObjectsToAdd.size() > 0)
        {
            for (GameObject go : gameObjectsToAdd)
            {
                gameObjects.add(go);
            }
            gameObjectsToAdd.clear();
        }

        //Initialzes the game if it hasn't
        if(sizeSet && !gameStarted)
        {
            gameStarted();
            gameStarted = true;
        }

        //Updates every active GameObjects
        for (GameObject go : gameObjects)
        {
            go.update(context,deltaTime);
        }

        FoodManager.getInstance().update(context,deltaTime);
        EnemyManager.getInstance().update(context,deltaTime);

        //Removes GameObjects
        if(gameObjectsToRemove.size() > 0)
        {
            for (GameObject go : gameObjectsToRemove)
            {
                if(gameObjects.contains(go))
                {
                    gameObjects.remove(go);
                }
                go.destroy(context);
            }
            gameObjectsToRemove.clear();
        }

        if(invalitate)
        {
            invalitate = false;
            ((Activity)context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(gameView != null)
                        gameView.invalidate();
                }
            });
        }
    }

    private void gameStarted()
    {
        Player player = new Player(context, R.drawable.pacman_eat_000,gameView,this);
        instantiate(player,gameView);
        player.Transform.setPosition(new Vector2Int((int)(size.getX())/2,(int)(size.getY()/2)));
        FoodManager.getInstance().intialize(this,context);
        EnemyManager.getInstance().intialize(this,context);
        this.player = player;
        SoundManager.getInstance().initialize(context);
        SoundManager.getInstance().playMusic(context,SoundEffectId.Music);
    }

    public void instantiate(GameObject gameObject,GameView gameView)
    {
        final GameView view = gameView;
        gameObjectsToAdd.add(gameObject);
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.invalidate();
            }
        });
    }

    public void destroy(GameObject gameObject, GameView gameView)
    {
        if(gameObject.Destroying)
            return;
        gameObjectsToRemove.add(gameObject);

        gameObject.Destroying = true;
    }

    public void setSize(int height, int width)
    {
        size = new Vector2Int(width,height);
        sizeSet = true;
    }

    public Player getPlayer() {
        return player;
    }

    public void killPlayer()
    {
        gameObjectsToRemove.add(player);
        Data.setPause(true);
        Data.gameEnding = true;

        final Context c = context;
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout pauseLayout = ((GameActivity)c).findViewById(R.id.scoreMenu);
                pauseLayout.setVisibility(View.VISIBLE);
                ImageView panel = ((GameActivity)c).findViewById(R.id.pausePanel);
                panel.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void reset(Context context) {
        for (GameObject go : gameObjects)
        {
            go = null;
        }
        gameObjects.clear();
        gameObjectsToRemove.clear();
        gameObjectsToAdd.clear();
        EnemyManager.getInstance().reset(context);
        FoodManager.getInstance().reset(context);
    }

    @Override
    public void destroy(Context context) {
        player = null;
        timer.cancel();
        timer = null;
        gameView = null;
    }

    public void invalidate()
    {
        invalitate = true;
    }

}
