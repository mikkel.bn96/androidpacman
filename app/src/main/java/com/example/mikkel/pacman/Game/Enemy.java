package com.example.mikkel.pacman.Game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.mikkel.pacman.R;
import com.example.mikkel.pacman.Utils.BoxCollider;
import com.example.mikkel.pacman.Utils.GameObject;
import com.example.mikkel.pacman.Utils.MathUtils;
import com.example.mikkel.pacman.Utils.Vector2;
import com.example.mikkel.pacman.Utils.Vector2Int;
import com.example.mikkel.pacman.Views.GameView;

public class Enemy extends GameObject {
    public Enemy(android.content.Context context, int bitmapId, GameView gameView, com.example.mikkel.pacman.Game.Game game) {
        super(context, bitmapId, gameView, game);

    }

    private CardinalDirection movementDirection;
    private boolean move;

    public void startMovement(CardinalDirection startPos)
    {
        switch (startPos) {
            case North:
                movementDirection = CardinalDirection.South;
                break;
            case West:
                movementDirection = CardinalDirection.East;
                break;
            case East:
                movementDirection = CardinalDirection.West;
                break;
            case South:
                movementDirection = CardinalDirection.North;
                break;
        }
        move = true;

        Vector2Int colliderSize = movementDirection == CardinalDirection.East || movementDirection == CardinalDirection.West ?
                new Vector2Int(75,35) : new Vector2Int(75,35);
        addComponent(new BoxCollider(this,colliderSize));
    }

    @Override
    public void destroy(android.content.Context context) {

    }

    @Override
    public void draw(Canvas canvas) {
        Bitmap bitmap = getBitmap();
        Vector2Int position = Transform.getPosition();
        Paint paint = new Paint();
        canvas.drawBitmap(getBitmap(),position.getX() - getBitmap().getWidth()/2,position.getY() - getBitmap().getHeight()/2,paint);
    }

    @Override
    public void reset(Context context) {

    }

    @Override
    public void update(Context context, float deltaTime)
    {
        if(move)
        {
            Vector2 direction = new Vector2(1,1);
            switch (movementDirection) {
                case North:
                    direction = new Vector2(0,-1);
                    break;
                case West:
                    direction = new Vector2(-1,0);
                    break;
                case East:
                    direction = new Vector2(1,0);
                    break;
                case South:
                    direction = new Vector2(0,1);
                    break;
            }

            float speedValue = (float)context.getResources().getInteger(R.integer.enemySpeed) * deltaTime;
            Transform.setPosition(Transform.getPosition().plus(direction.multiply(speedValue).toVector2Int()));
            handleDestroyBehavior();
        }

    }

    private void handleDestroyBehavior()
    {
        switch (movementDirection) {
            case North:
                if(Transform.getPosition().getY() <= -getBitmap().getHeight())
                    Game.destroy(this,getGameView());
                break;
            case West:
                if(Transform.getPosition().getX() <= -getBitmap().getWidth())
                    Game.destroy(this,getGameView());
                break;
            case East:
                if(Transform.getPosition().getX() - getBitmap().getWidth() >= Game.getSize().getX())
                    Game.destroy(this,getGameView());
                break;
            case South:
                if(Transform.getPosition().getY() - getBitmap().getHeight() >= Game.getSize().getY())
                    Game.destroy(this,getGameView());
                break;
        }
    }

    @Override
    public void onCollision(GameObject collision) {
        super.onCollision(collision);
        if(Player.class.isInstance(collision))
        {
            Game.killPlayer();
        }
    }
}
