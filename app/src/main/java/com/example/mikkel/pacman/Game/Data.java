package com.example.mikkel.pacman.Game;

public class Data {
    private static boolean pause = false;
    public static int Score = 0;
    public static boolean gameEnding = false;
    public static int test;

    public static void reset()
    {
        Data.Score = 0;
        Data.pause = false;
        Data.gameEnding = false;
    }

    public static boolean getPause() {
        return pause;
    }

    public static void setPause(boolean pause) {
        Data.pause = pause;
        if(pause)
        {
            SoundManager.getInstance().pauseMusic();
            SoundManager.getInstance().pauseSounds();
        }
        else
        {
            SoundManager.getInstance().resumeMusic();
            SoundManager.getInstance().resumeSounds();
        }
    }
}
