package com.example.mikkel.pacman.Interfaces;

import com.example.mikkel.pacman.Utils.GameObject;

public interface OnCollision
{
    public void onCollision(GameObject collision);
}
