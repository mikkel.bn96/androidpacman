package com.example.mikkel.pacman.Game;

import android.content.Context;
import android.util.Log;

import com.example.mikkel.pacman.Interfaces.Destroyable;
import com.example.mikkel.pacman.Interfaces.Resetable;
import com.example.mikkel.pacman.Interfaces.Updateable;
import com.example.mikkel.pacman.Utils.GameObject;
import com.example.mikkel.pacman.Utils.ResourceUtils;
import com.example.mikkel.pacman.Utils.Vector2;
import com.example.mikkel.pacman.Utils.Vector2Int;
import com.example.mikkel.pacman.R;

import java.util.ArrayList;
import java.util.Random;

public class FoodManager implements Updateable,Resetable
{
    private static FoodManager ourInstance = new FoodManager();

    private Game game;
    private boolean initialized;
    private ArrayList<Food> activeFoodList = new ArrayList<>();
    private float spawmTimer;
    private Context context;

    public static FoodManager getInstance() {
        return ourInstance;
    }

    public static void setFoodManager(FoodManager foodManager)
    {
        ourInstance = foodManager;
    }

    Integer[] foodImages = {
            R.drawable.food1,
            R.drawable.food2,
            R.drawable.food3,
            R.drawable.food4,
            R.drawable.food5,
            R.drawable.food6,
            R.drawable.food7,
            R.drawable.food8,
            R.drawable.food9
    };

    public void intialize(Game game, Context context)
    {
        this.game = game;
        this.context = context;
        setSpawnTimer();
        initialized = true;
    }

    @Override
    public void update(Context context, float deltaTime)
    {
        if(!initialized || activeFoodList.size() >= context.getResources().getInteger(R.integer.maxFood))
            return;

        if(spawmTimer < 0)
        {
            Random random = new Random();
            int randomValue = random.nextInt(foodImages.length - 1);
            int imageId = foodImages[randomValue];
            Food food = new Food(context, (imageId),game.getGameView(),game);
            food.Transform.setPosition(getSpawnPosition(food));
            food.Transform.setSize(new Vector2(0.75f,0.75f));
            game.instantiate(food,game.getGameView());
            setSpawnTimer();
            activeFoodList.add(food);
        }
        else
        {
            spawmTimer -= deltaTime;
        }
    }

    public ArrayList<Food> getActiveFoodList() {
        return activeFoodList;
    }

    public void setActiveFoodList(ArrayList<Food> activeFoodList) {
        this.activeFoodList = activeFoodList;
    }

    private void setSpawnTimer()
    {
        float min = ResourceUtils.GetFloat(R.dimen.minFoodSpawnDuration,context.getResources());
        float max = ResourceUtils.GetFloat(R.dimen.maxFoodSpawnDuration,context.getResources());
        Random random = new Random();
        spawmTimer = min + random.nextFloat() * (max - min);
    }

    private Vector2Int getSpawnPosition(Food food)
    {
        int xMin = food.getBitmap().getWidth();
        int xMax = game.getSize().getX() - food.getBitmap().getWidth();
        int yMin = food.getBitmap().getHeight();
        int yMax = game.getSize().getY() - food.getBitmap().getHeight();

        Random random = new Random();

        int xPos = xMin + random.nextInt(xMax - xMin);
        int yPos = yMin + random.nextInt(yMax - yMin);
        return new Vector2Int(xPos,yPos);
    }

    @Override
    public void reset(Context context) {
        for (GameObject go : activeFoodList)
        {
            go = null;
        }
        activeFoodList.clear();
        initialized = false;
    }
}
