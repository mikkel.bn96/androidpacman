package com.example.mikkel.pacman.Game;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.mikkel.pacman.GameActivity;
import com.example.mikkel.pacman.Interfaces.Eatable;
import com.example.mikkel.pacman.R;
import com.example.mikkel.pacman.Utils.BoxCollider;
import com.example.mikkel.pacman.Utils.GameObject;
import com.example.mikkel.pacman.Utils.MathUtils;
import com.example.mikkel.pacman.Utils.Vector2;
import com.example.mikkel.pacman.Utils.Vector2Int;
import com.example.mikkel.pacman.Views.GameView;

public class Food extends GameObject
{

    public Food(android.content.Context context, int bitmapId, GameView gameView, com.example.mikkel.pacman.Game.Game game) {
        super(context, bitmapId, gameView, game);
        addComponent(new BoxCollider(this,new Vector2Int(50,50)));
    }

    @Override
    public void destroy(Context context) {
        final Context c = context;

        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((GameActivity)c).updateScore();
            }
        });

        if(FoodManager.getInstance().getActiveFoodList().contains(this))
            FoodManager.getInstance().getActiveFoodList().remove(this);
    }

    @Override
    public void draw(Canvas canvas) {
        Bitmap bitmap = getBitmap();
        Vector2Int position = Transform.getPosition();
        Paint paint = new Paint();
        canvas.drawBitmap(getBitmap(),position.getX() - getBitmap().getWidth()/2,position.getY() - getBitmap().getHeight()/2,paint);
    }

    @Override
    public void reset(Context context) {

    }

    @Override
    public void onCollision(GameObject collision) {
        super.onCollision(collision);
        if(Player.class.isInstance(collision))
        {
            Data.Score++;
            Game.destroy(this,getGameView());
            SoundManager.getInstance().playSound(SoundEffectId.Eat);
        }

    }
}
