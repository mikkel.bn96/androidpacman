package com.example.mikkel.pacman.Game;

public class HighscoreManager {
    private static final HighscoreManager ourInstance = new HighscoreManager();

    public int getHighscore() {
        return highscore;
    }

    private int highscore;

    public String getHighscoreName() {
        return highscoreName;
    }

    private String highscoreName;

    public static HighscoreManager getInstance() {
        return ourInstance;
    }

    private HighscoreManager() {
    }

    public void ApplyScore(int score, String name)
    {
        if(score > highscore)
        {
            highscore = score;
            highscoreName = name;
        }
    }

}
