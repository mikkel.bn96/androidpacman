package com.example.mikkel.pacman.Utils;

public class Vector2Int {

    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Vector2Int(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Vector2Int plus(Vector2Int v2)
    {
        return new Vector2Int(x + v2.getX(), y + v2.getY());
    }

    public Vector2Int minus(Vector2Int v2)
    {
        return new Vector2Int(x - v2.getX(), y - v2.getY());
    }

    public Vector2Int multiply(Vector2Int v2)
    {
        return new Vector2Int(x * v2.getX(),y * v2.getY());
    }

    public Vector2Int multiply(float value)
    {
        return new Vector2Int(x * (int)value,y * (int)value);
    }

    public Vector2Int inverse()
    {
        return new Vector2Int(-x,-y);
    }

    public Boolean isEqual(Vector2Int vector)
    {
        Boolean value = getX() == vector.getX() && getY() == vector.getX();
        return value;
    }

    public Vector2 toVector2()
    {
        return new Vector2(getX(),getY());
    }
}
