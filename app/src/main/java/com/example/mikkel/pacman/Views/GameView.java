package com.example.mikkel.pacman.Views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.example.mikkel.pacman.Game.Data;
import com.example.mikkel.pacman.Game.Game;
import com.example.mikkel.pacman.Utils.GameObject;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public class GameView extends View {
    private boolean instantiated;
    private int height,width;
    private Context context;
    private Game game;
    private int test;

    public GameView(Context context) {
        super(context);
        this.context = context;
        test = Data.test;
        Data.test++;

    }

    public GameView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

    }

    public GameView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;

    }

    public void instantiate(Game game)
    {
        this.game = game;
        instantiated = true;
        this.context = context;
        test = Data.test;
        Data.test++;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        height = canvas.getHeight();
        width = canvas.getWidth();
        canvas.drawColor(Color.DKGRAY);
        super.onDraw(canvas);

        if (instantiated)
        {
            game.setSize(height,width);

            CopyOnWriteArrayList<GameObject> list = new CopyOnWriteArrayList(game.getGameObjects());
            for (GameObject go : list)
            {
                if(go != null)
                    go.draw(canvas);
            }
        }
    }

}
