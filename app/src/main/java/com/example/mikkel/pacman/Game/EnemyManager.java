package com.example.mikkel.pacman.Game;

import android.content.Context;
import android.util.Log;

import com.example.mikkel.pacman.Interfaces.Destroyable;
import com.example.mikkel.pacman.Interfaces.Resetable;
import com.example.mikkel.pacman.Interfaces.Updateable;
import com.example.mikkel.pacman.R;
import com.example.mikkel.pacman.Utils.GameObject;
import com.example.mikkel.pacman.Utils.MathUtils;
import com.example.mikkel.pacman.Utils.ResourceUtils;
import com.example.mikkel.pacman.Utils.Vector2;
import com.example.mikkel.pacman.Utils.Vector2Int;

import java.util.ArrayList;
import java.util.Random;

public class EnemyManager implements Updateable,Resetable{
    private static EnemyManager ourInstance = new EnemyManager();

    private Game game;
    private boolean initialized;
    private float spawmTimer;
    private Context context;
    private float difficultyTimer;
    private float timeUntilMax;
    private ArrayList<Enemy> activeEnemies = new ArrayList<>();

    public static EnemyManager getInstance() {
        return ourInstance;
    }

    public static void setEnemyManager(EnemyManager ourInstance) {
        EnemyManager.ourInstance = ourInstance;
    }

    public void intialize(Game game, Context context)
    {
        this.game = game;
        this.context = context;
        setSpawnTimer();
        initialized = true;
        difficultyTimer = ResourceUtils.GetFloat(R.dimen.timeUntilMax,context.getResources());
        timeUntilMax = difficultyTimer;
        setSpawnTimer();
    }

    private void setSpawnTimer()
    {
        Random random = new Random();

        float difficultyValue = MathUtils.Clamp(timeUntilMax * difficultyTimer == 0 ? 0 : 1 / timeUntilMax * difficultyTimer,0,1);

        float minMin = ResourceUtils.GetFloat(R.dimen.minMinEnemySpawnDuration,context.getResources());
        float minMax = ResourceUtils.GetFloat(R.dimen.minMaxEnemySpawnDuration,context.getResources());
        float min = minMin + (minMax - minMin) * difficultyValue;

        float maxMin = ResourceUtils.GetFloat(R.dimen.maxMinEnemySpawnDuration,context.getResources());
        float maxMax = ResourceUtils.GetFloat(R.dimen.maxMaxEnemySpawnDuration,context.getResources());
        float max = maxMin + (maxMax - maxMin) * difficultyValue;

        spawmTimer = min + random.nextFloat() * (max - min);
    }

    private Vector2Int getSpawnPosition(Enemy enemy, CardinalDirection spawnDir)
    {
        int xMin = enemy.getBitmap().getWidth();
        int xMax = game.getSize().getX() - enemy.getBitmap().getWidth();
        int yMin = enemy.getBitmap().getHeight();
        int yMax = game.getSize().getY() - enemy.getBitmap().getHeight();

        Random random = new Random();

        int xPos = xMin + random.nextInt(xMax - xMin);
        int yPos = yMin + random.nextInt(yMax - yMin);

        if(spawnDir == CardinalDirection.North)
            yPos = -enemy.getBitmap().getHeight();
        else if (spawnDir == CardinalDirection.South)
            yPos = game.getSize().getY() + enemy.getBitmap().getHeight();
        else if (spawnDir == CardinalDirection.East)
            xPos = game.getSize().getX() + enemy.getBitmap().getWidth();
        else
            xPos = -enemy.getBitmap().getWidth();

        return new Vector2Int(xPos,yPos);
    }

    @Override
    public void update(Context context, float deltaTime)
    {
        if(!initialized)
            return;

        if(spawmTimer < 0)
        {
            Random random = new Random();

            CardinalDirection spawnDir = CardinalDirection.values()[random.nextInt(3)];
            Enemy enemy = new Enemy(context,R.drawable.misile,game.getGameView(),game);
            enemy.Transform.setSize(new Vector2(0.65f,0.65f));
            Vector2Int spawnPos = getSpawnPosition(enemy,spawnDir);
            enemy.Transform.setPosition(spawnPos);

            switch (spawnDir) {
                case North:
                    enemy.Transform.setRotation(new Vector2(0,1));
                    break;
                case West:
                    enemy.Transform.setRotation(new Vector2(1,0));
                    break;
                case East:
                    enemy.Transform.setRotation(new Vector2(-1,0));
                    break;
                case South:
                    enemy.Transform.setRotation(new Vector2(0,-1));
                    break;
            }
            Log.d("Test",enemy.getBitmap().getWidth() + " " + enemy.getBitmap().getHeight());
            game.instantiate(enemy,game.getGameView());
            setSpawnTimer();
            enemy.startMovement(spawnDir);
            activeEnemies.add(enemy);
        }
        else
        {
            spawmTimer -= deltaTime;
        }

        if(difficultyTimer > 0)
        {
            difficultyTimer -= deltaTime;
            if(difficultyTimer < 0)
                difficultyTimer = 0;
        }
    }

    public ArrayList<Enemy> getActiveEnemies() {
        return activeEnemies;
    }

    public void setActiveEnemies(ArrayList<Enemy> activeEnemies) {
        this.activeEnemies = activeEnemies;
    }

    @Override
    public void reset(Context context) {
        for (GameObject go : activeEnemies)
        {
            go = null;
        }
        activeEnemies.clear();
        initialized = false;
    }
}
