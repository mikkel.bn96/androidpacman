package com.example.mikkel.pacman.Utils;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.example.mikkel.pacman.R;


public class SwipeTouchListener implements View.OnTouchListener {
    private final GestureDetector gestureDetector;

    public SwipeTouchListener(Context context) {
        gestureDetector = new GestureDetector(context,new GestureListener(context));
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    public void onSwipe(Vector2Int direction)
    {

    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private Context context;

        public GestureListener(Context context) {
            this.context = context;
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            try {
                int swipeThreshold = context.getResources().getInteger(R.integer.swipe_threshold);
                int swipeVelocityThreshold = context.getResources().getInteger(R.integer.swipe_velocity_threshold);

                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > swipeThreshold && Math.abs(velocityX) > swipeVelocityThreshold) {
                        if (diffX > 0) //Right
                        {
                            onSwipe(new Vector2Int(1,0));
                        } else //Left
                        {
                            onSwipe(new Vector2Int(-1,0));

                        }
                        result = true;
                    }
                } else if (Math.abs(diffY) > swipeThreshold && Math.abs(velocityY) > swipeVelocityThreshold) {
                    if (diffY > 0) //Up
                    {
                        onSwipe(new Vector2Int(0,-1));

                    } else //Down
                    {
                        onSwipe(new Vector2Int(0,1));

                    }
                    result = true;
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
            return result;
        }
    }
}
