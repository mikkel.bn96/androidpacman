package com.example.mikkel.pacman;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mikkel.pacman.Game.HighscoreManager;
import com.example.mikkel.pacman.Utils.MathUtils;
import com.example.mikkel.pacman.Utils.Vector2;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button startButton = findViewById(R.id.startButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent startGameIntent = new Intent(getApplicationContext(),GameActivity.class);
                startActivity(startGameIntent);
                finish();
            }
        });

        Button quitButton = findViewById(R.id.quitButton);
        quitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.exit(1);
            }
        });

        if(HighscoreManager.getInstance().getHighscore() > 0)
        {
            LinearLayout highscoreLayout = findViewById(R.id.highscorePanel);
            highscoreLayout.setVisibility(View.VISIBLE);

            TextView playerName = findViewById(R.id.playerName);
            playerName.setText(HighscoreManager.getInstance().getHighscoreName());

            TextView playerScore = findViewById(R.id.highscoreScore);
            playerScore.setText(String.valueOf(HighscoreManager.getInstance().getHighscore()));
        }
    }

}
