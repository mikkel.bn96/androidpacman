package com.example.mikkel.pacman.Game;

import com.example.mikkel.pacman.Utils.GameObject;

public class Animation
{
    private String name;
    private int fps;
    private int[] frames;

    public Animation(String name, int fps, int[] frameIds)
    {
        this.name = name;
        this.fps = fps;
        this.frames = frameIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFps() {
        return fps;
    }

    public void setFps(int fps) {
        this.fps = fps;
    }

    public int[] getFrames() {
        return frames;
    }

    public void setFrames(int[] frames) {
        this.frames = frames;
    }

    public void applyAnmation(GameObject gameObject, int currentFrame)
    {
        gameObject.setBitmapSource(getFrames()[currentFrame]);
    }
}
