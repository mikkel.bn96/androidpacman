package com.example.mikkel.pacman.Game;

import android.content.Context;

import com.example.mikkel.pacman.Interfaces.Updateable;
import com.example.mikkel.pacman.Utils.Component;
import com.example.mikkel.pacman.Utils.GameObject;

public class Animator extends Component
{
    private GameObject gameObject;
    private boolean running;
    private Animation currentAnimation;
    private int currentFrame;
    private float animationTimer;

    public Animator(GameObject gameObject)
    {
        this.gameObject = gameObject;
    }

    @Override
    public void update(Context context, float deltaTime)
    {
        if(!running || currentAnimation == null || currentAnimation.getFrames().length <= 0)
            return;

        if(animationTimer > 0)
        {
            animationTimer -= deltaTime;
        }
        else
        {
            setFrame(currentAnimation);
        }
    }

    public void startAnimation(Animation animation)
    {
        currentFrame = 0;
        setFrame(animation);
        running = true;
        currentAnimation = animation;
    }

    private void setFrame(Animation animation)
    {
        animation.applyAnmation(gameObject,currentFrame);
        animationTimer = 1f / animation.getFps();
        currentFrame ++;
        if(currentFrame >= animation.getFrames().length)
            currentFrame = 0;
    }

    public void play()
    {
        running = true;
    }

    public void stop()
    {
        running = false;
    }
}
