package com.example.mikkel.pacman.Utils;

import android.content.Context;

import com.example.mikkel.pacman.Interfaces.OnCollision;

import java.util.concurrent.CopyOnWriteArrayList;

public class BoxCollider extends Component
{
    private Vector2Int size;
    private GameObject gameObject;

    public BoxCollider(GameObject gameObject, Vector2Int size) {
        this.size = size;
        this.gameObject = gameObject;
    }

    public Vector2Int getSize() {
        return size;
    }

    public void setSize(Vector2Int size) {
        this.size = size;
    }

    @Override
    public void update(Context context, float deltaTime) {
        super.update(context, deltaTime);

        CopyOnWriteArrayList<GameObject> gameObjects = new CopyOnWriteArrayList(gameObject.Game.getGameObjects());
        for (GameObject go : gameObjects)
        {
            BoxCollider collider = go.getComponent(BoxCollider.class);
            if(collider != null)
            {
                Vector2Int thisPos = gameObject.Transform.getPosition();
                Vector2Int pos = collider.gameObject.Transform.getPosition();
//                if(pos.getX() - collider.size.getX() >= thisPos.getX() - size.getX() ||
//                        pos.getX() + collider.size.getX() <= thisPos.getX() + size.getX() ||
//                        pos.getY() - collider.size.getY() >= thisPos.getY() - size.getY()
//                        || pos.getY() + collider.size.getY() >= thisPos.getY() + size.getY())
//                {
//                    collider.collision(gameObject);
//                    collision(collider.gameObject);
//                }
                if(intersects(collider))
                {
                    collider.collision(gameObject);
                    collision(collider.gameObject);
                }
            }
        }
    }

    public void collision(GameObject collision)
    {
        gameObject.onCollision(collision);
    }

    public int left()
    {
        return gameObject.Transform.getPosition().getX() - size.getX();
    }

    public int right()
    {
        return gameObject.Transform.getPosition().getX() + size.getX();
    }

    public int top()
    {
        return gameObject.Transform.getPosition().getY() - size.getY();
    }

    public int bottom()
    {
        return gameObject.Transform.getPosition().getY() + size.getY();
    }

    public boolean intersects(BoxCollider target)
    {
//        if(target.right() > left() || target.left() < right())
//            return  false;
//        if(target.bottom() > top() || target.top() < bottom())
//            return false;
//
//        return true;
        return !(target.left() > right()
                || target.right() < left()
                || target.top() > bottom()
                || target.bottom() < top());
    }
}
