package com.example.mikkel.pacman.Game;

import com.example.mikkel.pacman.R;

public enum SoundEffectId
{
    Eat(R.raw.eat),
    Music(R.raw.off_limit);

    private int index;
    SoundEffectId(int index) {
        this.index = index;
    }

    public int value()
    {
        return this.index;
    }
}
