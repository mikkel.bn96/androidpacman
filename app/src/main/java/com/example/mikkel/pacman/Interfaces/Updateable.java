package com.example.mikkel.pacman.Interfaces;

import android.content.Context;

public interface Updateable
{
    public void update(Context context, float deltaTime);
}
