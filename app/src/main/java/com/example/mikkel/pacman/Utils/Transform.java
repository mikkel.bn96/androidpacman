package com.example.mikkel.pacman.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import com.example.mikkel.pacman.Game.Game;
import com.example.mikkel.pacman.Views.GameView;

public class Transform
{
    GameView view;
    Game game;
    GameObject gameObject;

    public Transform(GameView view, Game game, GameObject gameObject)
    {
        this.view = view;
        this.game = game;
        this.gameObject = gameObject;
    }

    private Vector2Int position = new Vector2Int(0,0);
    private Vector2 size = new Vector2(1,1);
    private Vector2 rotation = new Vector2(0,0);

    public Vector2Int getPosition() {
        return position;
    }

    public void setPosition(Vector2Int position) {
        this.position = position;
        game.invalidate();
    }

    public Vector2 getSize() {
        return size;
    }

    public void setSize(Vector2 size) {
        this.size = size;
        setRotation(getRotation());
        game.invalidate();
    }

    public Vector2 getRotation() {
        return rotation;
    }

    public void setRotation(Vector2 rotation) {
        this.rotation = rotation;
        Matrix matrix = new Matrix();
        float rotaionDegree = (float) Math.toDegrees((Math.atan2(rotation.getY(),rotation.getX())));
        matrix.setRotate(rotaionDegree);

        Bitmap bitmap = BitmapFactory.decodeResource(gameObject.Context.getResources(),gameObject.getBitmapSource());
        int width = (int)(bitmap.getWidth() * size.getX());
        int heigth = (int)(bitmap.getHeight() * size.getY());
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap,width,heigth,true);
        gameObject.setBitmap(Bitmap.createBitmap(scaledBitmap,0,0,scaledBitmap.getWidth(),scaledBitmap.getHeight(),matrix,true));
    }
}
